<?php 
require_once '../model/produit.php';
require_once 'connect.php';
interface  IRepoProduit{
public function ajouterProduit($produit):void;
public function modifierProduit($id,$produit):void;
public function supprimerProduit($id):void;
public function rechercherProduit($id):Produit;
public function getTousProduit():array;

}

class Repoproduit implements IRepoProduit {
    public $pdoObj;
    public function __construct()
    {
        $this->pdoObj= Connect::connect_to_db();
    }
    public function ajouterProduit($produit): void
    {
        $stmt=$this->pdoObj->prepare('insert into produit values (:id,:designation,:prix_unitaire)');
        $stmt->execute([':id'=>$produit->get_id(), ':designation' => $produit->get_designation(), ":prix_unitaire" => $produit->get_prix_unitaire()]);
    }
    public function modifierProduit($id,$produit): void
    {
        $stmt = $this->pdoObj->prepare("UPDATE produit
		SET designation = :designation, prix_unitaire = :prix_unitaire
		WHERE id = :id;");
        $stmt->execute([':id'=>$produit->get_id(), ':designation' => $produit->get_designation(), ":prix_unitaire" => $produit->get_prix_unitaire()]);

    }
    public function supprimerProduit($id): void
    {
        $stmt = $this->pdoObj->prepare("DELETE from produit
		WHERE id = :id;");
        $stmt->execute([':id'=>$id]);
    }
    public function rechercherProduit($id):Produit
    {
        $stmt = $this->pdoObj->prepare("select * from produit
		WHERE id = :id;");
        $stmt->execute([':id'=>$id]);
        $fetched = $stmt->fetch(PDO::FETCH_ASSOC);
        return new Produit($fetched["id"],$fetched["designation"],$fetched["prix_unitaire"]);
        
    }
    public function getTousProduit():array{
        $stmt = $this->pdoObj->prepare("select * from produit;");
        $stmt->execute();
        $fetched = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $fetched;
    }
}
$a = new Repoproduit();
print_r($a -> getTousProduit());

?>
