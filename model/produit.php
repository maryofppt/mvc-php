<?php

class Produit {
    private $id;
    private $designation;
    private $prix_unitaire;

    function __construct($id,$designation,$prix_unitaire) {
        $this->id = $id;
        $this->designation = $designation;
        $this->prix_unitaire = $prix_unitaire;
        
    }
    function get_id(){
        return $this->id;
    }
    function get_designation()
    {
        return $this->designation;
    }
    function get_prix_unitaire()
    {
        return $this->prix_unitaire;
    }


    function set_id($new_id) {  
        $this->id = $new_id;
    }
    function set_designation($new_designation)
    {
        $this->designation = $new_designation;
    }
    function set_prix_unitaire($new_prix_unitaire)
    {
        $this->prix_unitaire = $new_prix_unitaire;
    }}


?>