<?php
require_once "../repositry/irepo_produit.php";

class ProduitController {
	private IRepoProduit $repo_produit;

	public function __construct(){
		$this->repo_produit = new RepoProduit();
	}
	
	public function index(): array{
		return $this->repo_produit->getTousProduit();
	}

}
?>